package firefox.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import helper.ScreenshotHelper;
import io.github.bonigarcia.wdm.MarionetteDriverManager;

public class MarionetteTest {
	protected WebDriver driver;
	private ScreenshotHelper screenshotHelper;
	private String baseUrl;

	@BeforeClass
	public static void setupClass() {

		System.setProperty("webdriver.base.url", "http://www.google.com");
		MarionetteDriverManager.getInstance().setup();
	}

	@Before
	public void setupTest() {
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		driver = new FirefoxDriver(capabilities);
		baseUrl = System.getProperty("webdriver.base.url");
		screenshotHelper = new ScreenshotHelper();
	}

	@After
	public void saveScreenshotAndCloseBrowser() throws IOException {

		if (driver != null) {
			screenshotHelper.saveScreenshot("screenshot.png", driver);
			driver.quit();
		}
	}

	@Test
	public void page_Title_After_Search_Should_Begin_With_FESTinVAL() throws IOException {
		driver.get(baseUrl);

		assertEquals("il titolo della pagina dovrebbe essere google all' inizio del test.", "Google",
				driver.getTitle());

		WebElement searchField = driver.findElement(By.name("q"));
		final String testString = "FESTinVAL".toLowerCase();
		searchField.sendKeys(testString);
		searchField.submit();

		assertTrue("il titolo della pagina dovrebbe iniziare con il termine della ricerca dopo la ricerca",
				(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver d) {
						return d.getTitle().toLowerCase().startsWith(testString);
					}
				}));
	}

}
